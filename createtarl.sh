###############################################################################
#                                                                             #
#       SCRIPT NAME:    createtarl.sh                                          #
#                                                                             #
#       DESCRIPTION:    script for collating all the data and files required  #
#                       for a releasel                                         #
#                                                                             #
#   DATE     WHO ID       DESCRIPTION                                         #
#   ======== === ======== ===========                                         #
#   11/01/16 MR           Initial version                                     #
#   08/04/16 MR JDA-564   ITL extract profiles now local config               #
###############################################################################

dbdump -o function_access -w "function_id like 'MANDS%'"

dbdump -o language_text -w "(EXISTS (SELECT NULL FROM client_group cg WHERE cg.client_group = CASE WHEN label LIKE 'WTT%' THEN SUBSTR(label,4,instr(label,chr(95),1,1)-4) ELSE SUBSTR(label,1,instr(label,chr(95),1,1)-1) END) OR label LIKE 'MANDS%' OR label IN ('WLKITLInvalid Scan')) AND label NOT IN ('DEFAULT_VALUE')"

dbdump -o lookup_table -w "type = 'lookup_inv_trans_type' and label = 'Invalid Scan'"
dbdump -o system_profile -w "profile_id like '-ROOT-_USER_%'"
dbdump -o user_defined_field_data
dbdump -o user_defined_field_sql
dbdump -o location_zone -w "zone_1 in ('QC','HOLDING','REWORK','SORTING')"
dbdump -o mode_of_transport
dbdump -o itl_code_reports
dbdump -o itl_code_report_params
dbdump -o client_group
dbdump -o client_group_clients
dbdump -o user_defined_table -w "client_group IS NOT NULL"
dbdump -o user_defined_field -w "client_group IS NOT NULL"

mergerulesdatautil -s -i TEMPLATE -t1
mergerulesdatautil -s -i TEMPLATE -t3
mergerulesdatautil -s -i TEMPLATE -t5
mergerulesdatautil -s -i TEMPLATE -t6
schedulerdatautil -s
screenlayoutdatautil -s -y
rdtscreendatautil -s -g SCREEN-XD
userreportsdatautil -s

rm releasell.tar

tar -cvf releasel.tar b_*.sql
tar -rvf releasel.tar s_*.sql
tar -rvf releasel.tar data_updates.sql
tar -rvf releasel.tar dcsdba_grants.sql
tar -rvf releasel.tar function_access.dat
tar -rvf releasel.tar language_text.dat
tar -rvf releasel.tar location_zone.dat
tar -rvf releasel.tar lookup_table.dat
tar -rvf releasel.tar mands_autopick.sql
tar -rvf releasel.tar mands_grants.sql
tar -rvf releasel.tar mands_schema.sql
tar -rvf releasel.tar mands_views.sql
tar -rvf releasel.tar merge_rules_order_site_TEMPLATE.dmp
tar -rvf releasel.tar merge_rules_rdt_screen_site_TEMPLATE.dmp
tar -rvf releasel.tar merge_rules_upi_receipt_site_TEMPLATE.dmp
tar -rvf releasel.tar releasel1.0.sh
tar -rvf releasel.tar system_profile.dat
tar -rvf releasel.tar user_defined_field_data.dat
tar -rvf releasel.tar user_defined_field_sql.dat
tar -rvf releasel.tar scheduler_data_all.dmp
tar -rvf releasel.tar merge_rules_delivery_site_TEMPLATE.dmp
tar -rvf releasel.tar mode_of_transport.dat
tar -rvf releasel.tar mands_complete_delivery.sql
tar -rvf releasel.tar create_sequences.sql
tar -rvf releasel.tar create_directories.sql
tar -rvf releasel.tar complete_deliveries.sh
tar -rvf releasel.tar itl_code_reports.dat
tar -rvf releasel.tar itl_code_report_params.dat
tar -rvf releasel.tar screen_layout_data_all.dmp

cp $HOME/bin/schedulerextracts.sh $HOME/mands/
tar -rvf releasel.tar schedulerextracts.sh

tar -rvf releasel.tar rdt_screen_data_group_SCREEN-XD.dmp
tar -rvf releasel.tar client_group.dat
tar -rvf releasel.tar client_group_clients.dat
tar -rvf releasel.tar user_defined_table.dat
tar -rvf releasel.tar user_defined_field.dat
tar -rvf releasel.tar userreportsdatautil
tar -rvf releasel.tar user_reports.dmp
tar -rvf releasel.tar mands_auto_sd_reassign.sql
tar -rvf releasel.tar expand_to_sites.sql

cp $HOME/bin/createextract.sh $HOME/bin/createinvextract.sh $HOME/mands/
tar -rvf releasel.tar createextract.sh
tar -rvf releasel.tar createinvextract.sh

cp $HOME/bin/orchestration.sql $HOME/bin/extract_receipt.sql $HOME/mands/
tar -rvf releasel.tar orchestration.sql
tar -rvf releasel.tar extract_receipt.sql

cp $HOME/bin/create_pack_config_links.sh $HOME/bin/create_pack_config_links.sql $HOME/mands/
tar -rvf releasel.tar create_pack_config_links.sql
tar -rvf releasel.tar create_pack_config_links.sh

cp $HOME/bin/gift_card_extracts.sql $HOME/bin/gift_card_extracts.sh $HOME/mands/
tar -rvf releasel.tar gift_card_extracts.sql
tar -rvf releasel.tar gift_card_extracts.sh

cp $HOME/dcs/truncatelogs.conf $HOME/mands/
tar -rvf releasel.tar truncatelogs.conf

cp $HOME/bin/create_missing_deliveries.sh $HOME/bin/create_missing_deliveries.sql $HOME/mands/
tar -rvf releasel.tar create_missing_deliveries.sql
tar -rvf releasel.tar create_missing_deliveries.sh

cp $HOME/bin/retry_failed_upi_lines.sh $HOME/mands/
tar -rvf releasel.tar retry_failed_upi_lines.sh

cp $HOME/bin/purge_tables.sh $HOME/bin/purge_tables.sql $HOME/mands/
tar -rvf releasel.tar purge_tables.sh
tar -rvf releasel.tar purge_tables.sql

cp $HOME/bin/complete_deliveries.sh $HOME/bin/complete_deliveries.sql $HOME/mands/
tar -rvf releasel.tar complete_deliveries.sh
tar -rvf releasel.tar complete_deliveries.sql

tar -rvf releasel.tar b_rdt.sql 
tar -rvf releasel.tar s_rdt.sql
tar -rvf release1.tar releasel1.0.sh
