#!/bin/sh
###############################################################################
#                                                                             #
#       SCRIPT NAME:    release1.0.sh                                         #
#                                                                             #
#       DESCRIPTION:    script for deploying the template design into a       #
#                       environment                                           #
#                                                                             #
#   DATE     WHO ID       DESCRIPTION                                         #
#   ======== === ======== ===========                                         #
#   11/01/16 MR           Initial version                                     #                      
#   18/02/16 MR  JDA-127  Deploy sitable config to all available sites        #
#   19/02/16 MR           Change arguments for screenlayoutdatautil           #
#   22/02/16 MR  JDA-117  Import user reports                                 #
#   23/02/16 MR  JDA-134  Copy updated schedulerextracts.sh script            #
#   14/03/16 MR  JDA-335  Compile merge rules                                 #
#   04/04/16 MR           Search for package files rather than specify        #
#   08/04/16 MR  JDA-564  ITL extract profile now local config                #
###############################################################################
RELEASE_DIR="$HOME/mands"
OUTPUT_FILE="$RELEASE_DIR/output.txt"
LOG_FILE="$RELEASE_DIR/log.txt"
TMPFILE="$DCS_TMPDIR/tmpfile.$$"

if [[ $ORACLE_USR == *[@]* ]]; then
	export MANDS_USR="mands/mands123@$ORACLE_SID"
else
	export MANDS_USR="mands/mands123"
fi


usage ()
{
	(
	echo ""
	echo "Usage: [options]"
	echo ""
	#echo "[-s]	Copy master site data to other sites"
	#echo "[-c]	Create MANDS schema"
	#echo "[-r]	Import user reports"
	echo ""
	echo "This script should be used to deploy the config from the local"
	echo "environment (CATE1) to all other environments."
	echo ""
	echo "using the -s argument will delete existing site configuration data"
	echo "and replace it with the site configuration data exported from the"
	echo "TEMPLATE site for all sites found within the target environment."
	echo "The current local config that will be spawned for each site"
	echo "are :-"
	echo ""
	echo "  1) Delivery merge rules"
	echo "  2) Order merge rules"
	echo "  3) Pre-advice merge rules"
	echo "  4) Sku merge rules"
	echo "  5) Pack Config merge rules"
	echo "  6) UPI merge rules"
	echo "  7) RDT data rules"
	echo ""
	echo "Please use with caution"
	echo ""
	) | more
}


#create_mands_schema ()
#{
#	sqlplus -s /nolog << ! > $TMPFILE
#	connect $DCS_SYSDBA
#	set feedback off
#	set heading off
#	set pagesize 0
#	
#	SELECT substr(file_name,1,instr(file_name,'DATA01.dbf')-1) #path
#	FROM dba_data_files d
#	WHERE d.tablespace_name = 'DATA';
#!
#	DBPATH=`cat $TMPFILE`
#	rm $TMPFILE
#
#
#	echo "creating user in $DBPATH/mands_data.dbf"
#	sqlplus -s /nolog << ! > $TMPFILE
#		connect $DCS_SYSDBA
#		@mands_schema.sql $DBPATH/mands_data.dbf
#!
#	check_for_errors "Could not create the MANDS schema !"
#
#	echo ""
#}
#
#check_for_errors()
#{
#	FOUND=`grep ORA- $TMPFILE`
#
#	if [ "$FOUND" != "" ]
#	then
#		echo ""
#		echo "$1"
#		echo ""
#		cat $TMPFILE
#		echo ""
#		exit 1
##	fi
#}
#
#check_for_username ()
#{
#
#if  [ -f $OUTPUT_FILE ]; then
#rm $OUTPUT_FILE;
#fi
#
#
#sqlplus -s $DCS_SYSDBA << ! >> "$OUTPUT_FILE"
#set feedback off
#set heading off
#set pagesize 0
#set linesize 255
#
#select username
#from dba_users
#where username = 'MANDS';
#
#exit;
#!
#if [ ! -z $(grep "MANDS" $RELEASE_DIR/output.txt) ]; then
#	 echo "Schema already exists";
#    else
#	create_mands_schema
#fi
#}

load_data ()
{
   echo ""
   echo "Loading data about to run"
   dbload -i system_profile.dat
   dbload -i function_access.dat
   dbload -i language_text.dat
   dbload -i location_zone.dat
   dbload -r -i user_defined_field_sql.dat
   dbload -r -i user_defined_field_data.dat
   dbload -i lookup_table.dat
   dbload -r -i mode_of_transport.dat
   dbload -i itl_code_reports
   dbload -i itl_code_report_params
   dbload -i client_group
   dbload -i client_group_clients
   dbload -i user_defined_table
   dbload -i user_defined_field
}

create_mands_objects ()
{
#create the sequences first as these are required
#in some of the packages
   sqlplus -s /nolog << ! > $TMPFILE
	connect $MANDS_USR

	@create_sequences.sql
	@mands_views.sql
!

	check_for_errors "Failed to create mands objects !"
	cleanup

#Now create all of the package specs
for file in $HOME/mands/s_*sql
do
	sqlplus -s /nolog << ! > $TMPFILE
	connect $MANDS_USR
	@$file
!
	check_for_errors "Failed to create mands objects !"
        cleanup
done

#Now create all of the package bodys
for file in $HOME/mands/b_*sql
do
        sqlplus -s /nolog << ! > $TMPFILE
        connect $MANDS_USR
        @$file
!
        check_for_errors "Failed to create mands objects !"
        cleanup
done

#Finally, create the views and apply the correct grants
	sqlplus -s /nolog << ! > $TMPFILE
	connect $MANDS_USR
        @mands_views.sql
        @mands_grants.sql
!
        check_for_errors "Failed to create mands objects !"
        cleanup



}

do_language_updates ()
{
	echo ""
	echo "Performing data updates"
	sqlplus -s /nolog << ! > $TMPFILE
	connect $ORACLE_USR 
		@create_directories.sql
	        @data_updates.sql
		@dcsdba_grants.sql
!
	check_for_errors "Errors were encountered whilst performing dcsdba updates !"
	cleanup
}

load_merge_rules ()
{
	echo ""
	echo "Loading data utilities"
	mergerulesdatautil -l -i TEMPLATE -t6
	mergerulesdatautil -l -i TEMPLATE -t1
	mergerulesdatautil -l -i TEMPLATE -t3
        mergerulesdatautil -l -i TEMPLATE -t5
        schedulerdatautil -l -y
	screenlayoutdatautil -l -x -y
	rdtscreendatautil -l -g SCREEN-XD
}



cleanup ()
{
	rm -r $TMPFILE
}
cd $RELEASE_DIR

delploy_site_config()
{
	sqlplus -s /nolog << ! > $TMPFILE
		connect $ORACLE_USR
	@expand_to_sites.sql
!
	check_for_errors "There was an issue expanding the template designs to the other sites !"
	cleanup
}
#
# Main program
#

while getopts scr? 2> /dev/null ARG
do
	case $ARG in
		s)	CREATESITEDATA="TRUE";;

		c)	CREATESCHEMA="TRUE";;

		r)	IMPORTREPORTS="TRUE";;

		?)	usage
			exit 1;;
	esac
done



#Check if the mands schema exists in the database
if [ "$CREATESCHEMA" = "TRUE" ]
then
	check_for_username
fi

#Perform the language text updates and apply dcsdba grants
echo "Performing language text updates"
do_language_updates

#Now load all the mands data
echo "Loading mands data"
load_data


#Create the mands packages
echo "loading the mands packages"
create_mands_objects

#Make sure the scripts folder exists
if [[ ! -e $HOME/dlx/Scripts ]];
then
	mkdir -p $HOME/dlx/Scripts
fi

cp mands_autopick.sql $DCS/server/run/reports/en_gb/
cp mands_complete_delivery.sql $DCS/server/run/reports/en_gb/
cp mands_auto_sd_reassign.sql $DCS/server/run/reports/en_gb/
mv $HOME/mands/schedulerextracts.sh $HOME/bin
mv $HOME/mands/userreportsdatautil $HOME/bin
mv $HOME/mands/createinvextract.sh $HOME/bin
mv $HOME/mands/createextract.sh $HOME/bin
mv $HOME/mands/orchestration.sql $HOME/bin
mv $HOME/mands/extract_receipt.sql $HOME/bin
mv $HOME/mands/create_pack_config_links.sh $HOME/bin
mv $HOME/mands/create_pack_config_links.sql $HOME/bin
mv $HOME/mands/gift_card_extracts.sql $HOME/bin
mv $HOME/mands/gift_card_extracts.sh $HOME/bin
mv $HOME/mands/truncatelogs.conf $HOME/dcs/truncatelogs.conf
mv $HOME/mands/create_missing_deliveries.sh $HOME/bin/
mv $HOME/mands/create_missing_deliveries.sql $HOME/bin/
mv $HOME/mands/purge_tables.sh $HOME/bin/
mv $HOME/mands/purge_tables.sql $HOME/bin/
mv $HOME/mands/retry_failed_upi_lines.sh $HOME/bin/
mv $HOME/mands/complete_deliveries.sh $HOME/bin/
mv $HOME/mands/complete_deliveries.sql $HOME/bin/


#Now load all of the merge rules
load_merge_rules

if [ "$CREATESITEDATA" = "TRUE" ]
then
	echo "Deploying site config"
	delploy_site_config
fi

if [ "$IMPORTREPORTS" = "TRUE" ]
then
	echo "Importing user reports"
	userreportsdatautil -l
fi

#Stop and start the merge daemons so they pick up the new merge rules

echo ""
echo "Stopping merge daemons"
telsia stop ifdae

echo "Starting merge daemons"
telsia start ifdae

echo "Complete!!!"
